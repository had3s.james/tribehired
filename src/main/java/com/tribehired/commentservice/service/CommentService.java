package com.tribehired.commentservice.service;

import com.tribehired.commentservice.dto.CommentDTO;

import java.util.List;

public interface CommentService {
    List<CommentDTO> getComment();
    List<CommentDTO> getComment(long userId, CommentDTO filter_commentDTO);
}
