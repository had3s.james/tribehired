package com.tribehired.commentservice.service;

import com.tribehired.commentservice.dto.PostDTO;
import com.tribehired.commentservice.dto.TopPostDTO;

import java.util.List;

public interface PostService {
    List<PostDTO> getPost();
    PostDTO getPost(long postId);
    List<TopPostDTO> getTopPost();
}
