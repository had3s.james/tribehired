package com.tribehired.commentservice.service.impl;
import com.tribehired.commentservice.client.TribeHiredClient;
import com.tribehired.commentservice.dto.CommentDTO;
import com.tribehired.commentservice.dto.PostDTO;
import com.tribehired.commentservice.dto.TopPostDTO;
import com.tribehired.commentservice.service.CommentService;
import com.tribehired.commentservice.service.PostService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PostImpl implements PostService {

    private static final Logger LOG = LoggerFactory.getLogger(PostImpl.class);

    @Autowired
    TribeHiredClient tribeHiredClient;

    @Autowired
    CommentService commentService;

    @Override
    public List<PostDTO> getPost(){
        LOG.debug("Fetch All Post");
        return tribeHiredClient.getPosts();
    }

    @Override
    public PostDTO getPost(long postId){
        LOG.debug("Fetch Post with id "+postId);
        return tribeHiredClient.getPosts(postId);
    }

    @Override
    public List<TopPostDTO> getTopPost(){
        List<PostDTO> postDTOS = getPost();
        List<CommentDTO> commentDTOS = commentService.getComment();

        LOG.debug("Calculate Post comments");
        List<CommentDTO> tempCommentDTOS=new ArrayList<>();
        List<TopPostDTO> topPostDTOS=new ArrayList<>();
        int total_number_of_comments;
        for(PostDTO postDTO:postDTOS){
            total_number_of_comments=0;
            tempCommentDTOS.addAll(commentDTOS);
            for(CommentDTO commentDTO:tempCommentDTOS){
                if(commentDTO.getPostId()==postDTO.getId()){
                    total_number_of_comments++;
                }
            }
            topPostDTOS.add(
                    new TopPostDTO(
                            postDTO.getId(),
                            postDTO.getTitle(),
                            postDTO.getBody(),
                            total_number_of_comments
                    )
            );
            commentDTOS.removeAll(tempCommentDTOS);
        }

        return topPostDTOS;
    }
}
