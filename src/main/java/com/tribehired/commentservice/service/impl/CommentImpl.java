package com.tribehired.commentservice.service.impl;

import com.fasterxml.jackson.core.JsonParser;
import com.tribehired.commentservice.client.TribeHiredClient;
import com.tribehired.commentservice.dto.CommentDTO;
import com.tribehired.commentservice.dto.PostDTO;
import com.tribehired.commentservice.service.CommentService;
import com.tribehired.commentservice.service.PostService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Service
public class CommentImpl  implements CommentService {

    private static final Logger LOG = LoggerFactory.getLogger(CommentImpl.class);

    @Autowired
    TribeHiredClient tribeHiredClient;

    @Autowired
    PostService postService;

    @Override
    public List<CommentDTO> getComment(){
        LOG.debug("Fetch All Comment");
        return tribeHiredClient.getComment();
    }

    @Override
    public List<CommentDTO> getComment(long userId, CommentDTO filter_commentDTO){
        List<PostDTO> postDTOS=postService.getPost();
        List<CommentDTO> commentDTOS=getComment();

        LOG.debug("Filter Comment");
        List<CommentDTO> filterCommentDTOS=new ArrayList<>();
        for(PostDTO postDTO:postDTOS){
            //filter post that is belong to the user only
            if(postDTO.getUserId()==userId){
                for(CommentDTO commentDTO:commentDTOS){
                    //filter comment that is belong to the post only
                    if(commentDTO.getPostId()==postDTO.getId()) {
                        boolean filter = true;
                        if(filter_commentDTO.getBlacklist()!=null){
                            for(long id:filter_commentDTO.getBlacklist()) {
                                if (commentDTO.getId()== id){
                                    filter=false;
                                    break;
                                }
                            }
                        }
                        if (filter && filter_commentDTO.getPostId()>0) {
                            filter = filter_commentDTO.getPostId() == postDTO.getId();
                        }
                        if (filter && filter_commentDTO.getId()>0) {
                            filter = filter_commentDTO.getId() == commentDTO.getId();
                        }
                        if (filter && !StringUtils.isEmpty(filter_commentDTO.getName())) {
                            filter = commentDTO.getName().toLowerCase().contains(filter_commentDTO.getName().toLowerCase());
                        }
                        if (filter && !StringUtils.isEmpty(filter_commentDTO.getEmail())) {
                            filter = commentDTO.getEmail().toLowerCase().contains(filter_commentDTO.getEmail().toLowerCase());
                        }
                        if (filter && !StringUtils.isEmpty(filter_commentDTO.getBody())) {
                            filter = commentDTO.getBody().toLowerCase().contains(filter_commentDTO.getBody().toLowerCase());
                        }
                        if (filter) {
                            filterCommentDTOS.add(commentDTO);
                        }
                    }
                }
            }
        }
        return filterCommentDTOS;
    }
}
