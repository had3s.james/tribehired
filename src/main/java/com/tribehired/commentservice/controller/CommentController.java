package com.tribehired.commentservice.controller;

import com.tribehired.commentservice.dto.CommentDTO;
import com.tribehired.commentservice.service.CommentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import sun.security.pkcs.ParsingException;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value="/api/comment")
public class CommentController {

    private static final Logger LOG = LoggerFactory.getLogger(CommentController.class);

    @Autowired
    CommentService commentService;

    @GetMapping()
    public List<CommentDTO> getComment(@RequestParam(value = "postId",required = false) Long filter_postId,
                                       @RequestParam(value = "id",required = false) Long filter_Id,
                                       @RequestParam(value = "name",required = false) String filter_name,
                                       @RequestParam(value = "email",required = false) String filter_email,
                                       @RequestParam(value = "body",required = false) String filter_body) {
        LOG.info("Get All Comment");
        return commentService.getComment();
    }

    @GetMapping(value="{user_id}")
    public List<CommentDTO> getComment(@PathVariable("user_id") long userId,
                                       @RequestParam(value = "postId",required = false) Long filter_postId,
                                       @RequestParam(value = "id",required = false) Long filter_Id,
                                       @RequestParam(value = "name",required = false) String filter_name,
                                       @RequestParam(value = "email",required = false) String filter_email,
                                       @RequestParam(value = "body",required = false) String filter_body,
                                       @RequestParam(value = "blacklist",required = false) String filter_blacklists) {
        LOG.info("Get User {} Post Comment ",userId);
        return commentService.getComment(userId,filterComment(filter_postId,filter_Id,filter_name,
                filter_email,filter_body,filter_blacklists));
    }

    private CommentDTO filterComment(Long filter_postId,
                                     Long filter_Id,
                                     String filter_name,
                                     String filter_email,
                                     String filter_body,
                                     String filter_blacklists){
        CommentDTO filter_commentDTO=new CommentDTO();
        if(!StringUtils.isEmpty(filter_postId))
            filter_commentDTO.setPostId(filter_postId);
        if(!StringUtils.isEmpty(filter_Id))
            filter_commentDTO.setId(filter_Id);
        filter_commentDTO.setName(filter_name);
        filter_commentDTO.setEmail(filter_email);
        filter_commentDTO.setBody(filter_body);
        List<Long> filter_blacklist=new ArrayList<>();
        for(String blacklistId:filter_blacklists.split(",")){
            try {
                filter_blacklist.add(Long.parseLong(blacklistId));
            }catch (NumberFormatException e){
                LOG.error("failed to convert number");
            }
        }
        filter_commentDTO.setBlacklist(filter_blacklist);
        LOG.info("filter Comment {}",filter_commentDTO);
        return filter_commentDTO;
    }
}