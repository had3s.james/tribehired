package com.tribehired.commentservice.controller;

import com.tribehired.commentservice.dto.PostDTO;
import com.tribehired.commentservice.dto.TopPostDTO;
import com.tribehired.commentservice.service.PostService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value="/api/post")
public class PostController {

    private static final Logger LOG = LoggerFactory.getLogger(PostController.class);

    @Autowired
    PostService postService;

    @GetMapping()
    public List<PostDTO> getPost() {
        LOG.info("Get All Post");
        return postService.getPost();
    }

    @GetMapping(value="{post_id}")
    public PostDTO getPost(@PathVariable("post_id") long postId) {
        LOG.info("Get Post id "+postId);
        return postService.getPost(postId);
    }

    @GetMapping(value="top")
    public List<TopPostDTO> getTopPost(){
        LOG.info("Get Top Post");
        return postService.getTopPost();
    }
}