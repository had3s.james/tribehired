package com.tribehired.commentservice.client;

import com.tribehired.commentservice.dto.CommentDTO;
import com.tribehired.commentservice.dto.PostDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name="tribehired-devs", url= "${tribehired.comment.url}")
public interface TribeHiredClient {

    @GetMapping(value={"/comments"})
    List<CommentDTO> getComment();

    @GetMapping(value={"/posts/{post_id}"})
    PostDTO getPosts(@PathVariable("post_id") long postId);

    @GetMapping(value={"/posts"})
    List<PostDTO> getPosts();
}
