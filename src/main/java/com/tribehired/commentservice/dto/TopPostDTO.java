package com.tribehired.commentservice.dto;


import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(staticName = "of")
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@Data
public class TopPostDTO {
    private long post_id;
    private String post_title;
    private String post_body;
    private int total_number_of_comments;
}
