package com.tribehired.commentservice.dto;

import lombok.Data;

@Data
public class PostDTO {
    private long id;
    private long userId;
    private String title;
    private String body;
}
