package com.tribehired.commentservice.dto;

import lombok.Data;

import java.util.List;

@Data
public class CommentDTO {
    private long id;
    private long postId;
    private String name;
    private String email;
    private String body;
    private List<Long> blacklist;

}
